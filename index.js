// Require modules

const express = require("express");

const mongoose = require("mongoose");
const cors = require("cors");

// Create server
const app = express();
const port = 4000;

// Connect to out MOngoDB Databse
mongoose.connect("mongodb+srv://admin:admin@coursebooking.jrfta.mongodb.net/b183_to-do?retryWrites=true&w=majority", 
	{useNewUrlParser: true, useUnifiedTopology: true});

		// useNewUrlParser: true,
		// useUnifiedTopology: true


// Set notification for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
// Allow all resources to access our backend application.
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Listening to port
// This syntax will allow flexibility when using the application locally or as a hosted application (online).
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})